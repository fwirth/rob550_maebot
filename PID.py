# PID.py
# 
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time,signal
import lcm
import math 
import PID

from math import pi
from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t


class PID():
    def __init__(self, kp, ki, kd):
        # Main Gains
        self.kp = kp
        self.ki = ki
        self.kd = kd
            
        # Integral Terms
        self.iTerm = 0.0
        self.iTermMin = -0.2
        self.iTermMax = 0.2

        # Input (feedback signal)
        self.prevInput = 0.0
        self.input = 0.0

        # Reference Signals (signal and derivative!)
        self.dotsetpoint = 0.0          
        self.setpoint = 0.0


        # Output signal and limits
        self.output = 0.0
        self.outputMin = -0.4
        self.outputMax = 0.4

        # Update Rate
        self.updateRate = 0.1

        # Error Signals
        self.error = 0.0
        self.errordot = 0.0
        self.prevError = 0.0
        
        self.trim = 0.05

    def Compute(self):
        # IMPLEMENT ME!
        # Different then last project! 
        # Now you have a speed reference also!
        # Also Update Rate can be gone here or in SetTunnings function
        # (it is your choice!) But change functions consistently
        
        # To allow for velocity control using dotsetpoint values
	#self.setpoint = self.setpoint + self.dotsetpoint * self.updateRate
        
	self.error = self.setpoint - self.input
        self.errordot = (self.error - self.prevError)/self.updateRate
        ud = self.errordot*self.kd 
	# Calculate P term as kp * error (between distance set points)
        up = self.error*self.kp
        
        # Calculate I term as ki * error (between distance set points) + Prev I    
        self.iTerm += self.error*self.updateRate
   	 
	# Saturation criteria for I term
        if self.iTerm > self.iTermMax:
            self.iTerm = self.iTerm
        if self.iTerm < self.iTermMin:
            self.iTerm = self.iTerm
        
        # Calculate I term     
        ui = self.iTerm*self.ki
        
        self.output = up + ui + ud + self.trim
        if self.output > self.outputMax:
            self.output = self.outputMax
        if self.output < self.outputMin:
            self.output = self.outputMin
        #print self.output
        self.prevInput = self.input
        self.prevError = self.error
        #print (up,ui,ud,self.output,self.errordot)
	return (self.output, up, ui, ud)    
            
        
    # Accessory Function to Change Gains
    # Update if you are not using updateRate in Compute()
    def SetTunings(self, kp, ki, kd):
        self.kp = kp*self.updateRate
        self.ki = ki*self.updateRate 
        self.kd = kd*self.updateRate

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
                
    def SetUpdateRate(self, rateInSec):
        if (rateInSec == 0):
            self.updaterate = .1
        else:
            self.updateRate = rateInSec
        #print (self.updateRate)



class PIDController():
    def __init__(self):
     
        self.FLAG = 1

        # Create Both PID .0025,00001,00009
        self.leftCtrl =  PID(0.0025,0.0001,0.0009)#.00025
        self.rightCtrl = PID(0.0025,0.0001,0.0009)
        
        # LCM Subscribe
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
        lcmCommandSub = self.lc.subscribe("GS_VELOCITY_CMD", self.motorCommandHandler)
        lcmCommandSub = self.lc.subscribe("GS_PID_INIT", self.PIDInitHandler)

        signal.signal(signal.SIGINT, self.signal_handler)
 
        
        # Odometry 
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / 
                       (self.gearRatio * self.ticksPerRev)) 
                        # encoder ticks to distance [mm]

        # Initialization Variables
        self.InitialPosition = (0.0, 0.0)
        self.oldTime = 0.0
        self.startTime = 0.0
        
        self.LeftSpeed = 0.0
        self.RightSpeed = 0.0
        
        self.curEncPos = (0,0,0)    #(left,right,time)
        self.preEncPos = (0,0,0)   #(left,right,time)
        
        self.LeftWheelDistance = 0.0
        self.RightWheelDistance = 0.0
        self.DesiredDistance = 0.0
        self.DesiredSpeed = 0.0
        self.theta = 0.0
        self.DesiredAngle = 0.0
	self.Straight = True
	self.Turn = False

    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        leftInfo = (0,0,0,0)
        rightInfo = (0,0,0,0)
        # IMPLEMENT ME  
	if self.Turn:
		cmd.motor_left_speed = self.leftCtrl.Compute() - self.leftCtrl.trim     
		cmd.motor_right_speed = self.rightCtrl.Compute() - self.rightCtrl.trim
	else:
		leftInfo = self.leftCtrl.Compute()
                cmd.motor_left_speed = leftInfo[0]
                rightInfo = self.rightCtrl.Compute()
        	cmd.motor_right_speed = rightInfo[0]

                print self.LeftWheelDistance - self.RightWheelDistance, cmd.motor_left_speed-cmd.motor_right_speed, leftInfo[1]-rightInfo[1], leftInfo[2]-rightInfo[2],leftInfo[3]-rightInfo[3]
	
	# Extra case to stop as soon as overshoot and ensure other variables are saturated       
        if self.Straight:

		if abs(self.LeftWheelDistance) > abs(self.DesiredDistance):
        		cmd.motor_left_speed= 0
                	self.leftCtrl.dotsetpoint = 0
	                self.leftCtrl.setpoint = self.LeftWheelDistance
        	        self.leftCtrl.iTerm = 0
                        #self.DesiredDistance = 0
                        #self.DesiredDistance = 0
	
        	if abs(self.RightWheelDistance) > abs(self.DesiredDistance):
                	cmd.motor_right_speed = 0
                    	self.rightCtrl.dotsetpoint = 0
                    	self.rightCtrl.setpoint = self.RightWheelDistance
                    	self.rightCtrl.iTerm = 0
                        #self.DesiredDistance = 0
                        #print self.DesiredDistance
	else:
        	if abs( self.theta - self.DesiredAngle)*180/pi<2:
                        cmd.motor_left_speed= 0
                        self.leftCtrl.dotsetpoint = 0
                        self.leftCtrl.setpoint = self.LeftWheelDistance
                        self.leftCtrl.iTerm = 0
			cmd.motor_right_speed = 0
                        self.rightCtrl.dotsetpoint = 0
                        self.rightCtrl.setpoint = self.RightWheelDistance
                        self.rightCtrl.iTerm = 0
                        print "Desired Angle <= 0"
                        self.DesiredAngle = self.theta
                

        #print (self.LeftWheelDistance,self.RightWheelDistance)
        self.lc.publish("MAEBOT_DIFF_DRIVE", cmd.encode())
        
    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        # To allow build of prevEncPos
        if self.FLAG==1:
            self.curEncPos = (msg.encoder_left_ticks,msg.encoder_right_ticks,msg.utime)
            self.prevEncPos = self.curEncPos
            self.FLAG = 0

	# Compute total distance travelled on both wheels independently
        else:
	    self.curEncPos = (msg.encoder_left_ticks,msg.encoder_right_ticks,msg.utime)
            self.LeftWheelDistance = self.LeftWheelDistance + (self.curEncPos[0] - self.prevEncPos[0])*self.enc2mm
            self.RightWheelDistance = self.RightWheelDistance + (self.curEncPos[1] - self.prevEncPos[1])*self.enc2mm

            dt = self.curEncPos[2] - self.prevEncPos[2]*0.000001
            self.leftCtrl.SetUpdateRate((self.curEncPos[2] - self.prevEncPos[2])*.000001)
            self.rightCtrl.SetUpdateRate((self.curEncPos[2] - self.prevEncPos[2])*.000001)

            self.leftCtrl.input = self.LeftWheelDistance
            self.rightCtrl.input = self.RightWheelDistance
       
            if self.Turn:
            	self.theta = self.theta +  (-(self.curEncPos[0] - self.prevEncPos[0])*self.enc2mm +  (self.curEncPos[1] - self.prevEncPos[1])*self.enc2mm)/self.axleLengthMillimeters
            
            
            self.prevEncPos = self.curEncPos
        

    def motorCommandHandler(self,channel,data):
        msg = velocity_cmd_t.decode(data)
        # IMPLEMENT ME
        # Change to Receive the Message from Ground Station
        # And command the bot
        self.Turn = False
        self.Straight = True
        if (msg.Distance < 0):
            self.leftCtrl.dotsetpoint = -msg.FwdSpeed
            self.rightCtrl.dotsetpoint = -msg.FwdSpeed
            self.DesiredDistance = -msg.Distance + self.DesiredDistance
        elif (msg.Distance > 0):
            self.leftCtrl.dotsetpoint = msg.FwdSpeed
            self.rightCtrl.dotsetpoint = msg.FwdSpeed
            self.DesiredDistance = msg.Distance + self.DesiredDistance
        else:
            self.Turn = True
            self.Straight = False
            if (msg.Angle > 0):
                self.leftCtrl.dotsetpoint = -msg.AngSpeed# * ( self.axleLengthMillimeters/2)
                self.rightCtrl.dotsetpoint = msg.AngSpeed# * ( self.axleLengthMillimeters/2)
                self.DesiredAngle = msg.Angle * pi/180 + self.DesiredAngle
            else:
                self.leftCtrl.dotsetpoint = -msg.AngSpeed# *   (self.axleLengthMillimeters/2)
                self.rightCtrl.dotsetpoint = msg.AngSpeed# * (self.axleLengthMillimeters/2)
                self.DesiredAngle = msg.Angle * pi/180 + self.DesiredAngle
	print (self.LeftWheelDistance,self.RightWheelDistance)
	# Add code to use angular velocity and control bot

    def PIDInitHandler(self,channel,data):
	# Setting values based on input data
        msg = pid_init_t.decode(data)
        self.leftCtrl = PID(0,0,0)
        self.rightCtrl = PID(0,0,0)
	print self.leftCtrl.kp
        self.rightCtrl.SetTunings(msg.kp,msg.ki,msg.kd)
	self.leftCtrl.SetTunings(msg.kp,msg.ki,msg.kd)
        
        print (msg.kp,msg.ki,msg.kd)
	self.rightCtrl.iTerm = 0
	self.leftCtrl.iTerm = 0


    
    def Controller(self):
        # For now give a fixed command here
        # Later code to get from groundstation should be used
        # To Turn 90

	# Convert both to functions

	#Turn 90: DesiredDistance = 30, Speed = 50
	if self.Turn:
		self.DesiredSpeed = 50
		self.DesiredAngle = pi/2
		self.leftCtrl.SetTunings(0.0025,0.0,0.0)
		self.rightCtrl.SetTunings(0.0025,0.0,0.0)

        Ku = .25
        Tu = .014
        Kp = .3*Ku
	# To Go straight for a meter
        if self.Straight:
		self.DesiredSpeed = 100
        	self.DesiredDistance = 1000
		self.leftCtrl.SetTunings(.0005,.0005,-.000059/10)#.009,.04,0001
		self.rightCtrl.SetTunings(.0005,.0005,-.000059/10)#.011,.04,.001
	


        self.leftCtrl.setpoint = self.DesiredDistance

	if self.Turn:
        	self.leftCtrl.dotsetpoint = - self.DesiredSpeed
	else:
		self.leftCtrl.dotsetpoint = self.DesiredSpeed

        self.rightCtrl.setpoint = self.DesiredDistance
        self.rightCtrl.dotsetpoint = self.DesiredSpeed

        while(1):
            self.lc.handle()
        # IMPLEMENT ME
        # MAIN CONTROLLER
            self.publishMotorCmd()
        
        
        
        
       
    # Function to print 0 commands to motor when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0  
            self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        exit(1)

pid = PIDController()
pid.Controller()


