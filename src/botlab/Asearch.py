import heapq
import sys
import numpy as np
from  PIL import Image
# Add function to convert numpy into occupancy grids DONE
# Check to make sure that x and y coords are correct DONE
# Display output function by plotting values within the image and using center of occupancy grid DONE



class grid_cell:
	def __init__(self,x,y,occupied):
		self.occupied = occupied
		self.x = x	
		self.y = y
		self.path = None
		self.g = 0
		self.h = 0
		self.f = 0
		self.path = []
class Algo:
	def __init__(self):
		self.open = []
		heapq.heapify(self.open)
		self.visited = set()
		self.cell = []
		self.grid_cell_size = 9 
		self.grid_h = 600/self.grid_cell_size
		self.grid_w = 600/self.grid_cell_size
		self.map_name = 'map1.png'
		self.data_name = 'map1.npy'
		self.occupied_threshold = 245.0#200.0

	def display_grid(self):
		data = np.load(self.data_name)
		row = len(data)
		col = len(data[0])
		for x in range(row/self.grid_cell_size):
			for y in range(col/self.grid_cell_size):	
				if np.sum(data[x*self.grid_cell_size:(x)*self.grid_cell_size+(self.grid_cell_size),y*self.grid_cell_size:(y)*self.grid_cell_size+(self.grid_cell_size)])/(self.grid_cell_size*self.grid_cell_size) < self.occupied_threshold:
					print "1",
				else:
					print "0",
			print "-"
		




	def init_grid(self):
		
		data = np.load(self.data_name)
		print data
		row = len(data)
		col = len(data[0])
		for x in range(row/self.grid_cell_size):
			for y in range(col/self.grid_cell_size):	
				if np.sum(data[x*self.grid_cell_size:(x)*self.grid_cell_size+(self.grid_cell_size),y*self.grid_cell_size:(y)*self.grid_cell_size+(self.grid_cell_size)])/(self.grid_cell_size*self.grid_cell_size) < self.occupied_threshold:
					occupied = True
				else:
					occupied = False
				self.cell.append(grid_cell(x,y,occupied))

		#walls = ((0,0),(1,0),(2,0),(1,1),(2,1),(1,3))
		#for x in range(self.grid_w):
			#for y in range(self.grid_h):
				#if (x,y) in walls:
					#occupied = True
				#else:
					#occupied = False
				#self.cell.append(grid_cell(x,y,occupied))
		self.start = self.get_cell(33,33)#(10,37)
		self.end = self.get_cell(65,26)#(73,37)
	
	def get_cell(self,x,y):
		return self.cell[x*self.grid_h+y]	

	def path_find(self):
                # Build wall thicker
                cells=[]
                for x in range(self.grid_w):
                        for y in range(self.grid_h):
                                xx = self.get_cell(x,y)
                                if xx.occupied:
                                        cells.append(self.get_cell(xx.x, xx.y))


                for mm in cells:
                        if mm.x < self.grid_w-1:
                                temp = self.get_cell(mm.x+1, mm.y)
                                temp.occupied = True
                        #if mm.y > 0:
                        #        temp = (self.get_cell(mm.x, mm.y-1))
                        #        temp.occupied = True
                        if mm.x > 0:
                                print mm.x-1,mm.y
                                temp = self.get_cell(mm.x-1, mm.y)
                                temp.occupied = True
                        if mm.y < self.grid_h-1:
                                temp =(self.get_cell(mm.x, mm.y+1))
                                temp.occupied = True






		heapq.heappush(self.open,(self.start.f,self.start))
		while(len(self.open)):
			f,cell = heapq.heappop(self.open)
			self.visited.add(cell)
			if cell is self.end:
				#self.display_path()
				break
			adj_cells = self.get_adjacent_cells(cell)
			for neighbour  in adj_cells:
				if not neighbour.occupied and neighbour	not in self.visited:
					if(neighbour.f,neighbour) in self.open:
						if neighbour.g > cell.g + 1:
							self.update_cell(neighbour,cell)
					else:
						self.update_cell(neighbour,cell)
						heapq.heappush(self.open,(neighbour.f,neighbour))

	def display_path(self):
		
		i = Image.open(self.map_name)
		pixelMap = i.load()
		pixelMap[18,5]=(150,0,0)
		cell = self.end
		ee=[((cell.y*self.grid_cell_size+5)*10.0,(cell.x*self.grid_cell_size+5)*10.0)]
		while cell.parent is not self.start:
        		cell = cell.parent
			ee.append(((cell.y*self.grid_cell_size+5)*10.0,(cell.x*self.grid_cell_size+5)*10.0))
        		#print 'path: cell: %d,%d' % (cell.y, cell.x)
		ee.append(((self.start.y*self.grid_cell_size+5)*10.0,(self.start.x*self.grid_cell_size+5)*10.0))
		ee.reverse()

		# Change to bot coords
		self.path=[[3000.0,3000.0]]
		x=3000.0
		y=3000.0
		count = 1
		for num in ee:
			if count==1:
				count=count+1
				x_prev= num[0]
				y_prev=num[1]
			else:
				x_temp =  num[0] - x_prev 
				y_temp = y_prev - num[1] 
				self.path.append([x+x_temp,y+y_temp])
				x=x+x_temp
				y=y+y_temp
				x_prev = num[0]
				y_prev = num[1]			
		#print self.path
		#print "\n"
		# Remove intermediate coords
		count =1
		X_FLAG=True
		Y_FLAG=True
		self.final = [[3000.0,3000.0]]
		for coords in self.path:
			if count==1:
				count = count + 1
				prev = coords
				X_FLAG=True
				Y_FLAG=True
			else:
				if (X_FLAG and Y_FLAG):
				# chain of Xs
					if coords[0]==prev[0]:
						x=coords[0]
						Y_FLAG=False
					elif coords[1]==prev[1]:
						y=coords[1]
						X_FLAG=False
				else:
					if(X_FLAG):
						x = prev[0]
						print prev[1],coords[1]
						if(coords[1]==prev[1]):
							y=coords[1]
							self.final.append([x,coords[1]])
							Y_FLAG=True
							if not coords[0]==prev[0]:
								X_FLAG=False
					elif(Y_FLAG):
						y = prev[1]
						if(coords[0]==prev[0]):
							x=coords[0]
							self.final.append([coords[0],y])
							X_FLAG=True
							if not coords[0]==prev[0]:
								Y_FLAG=False
				prev = coords					
				
		self.final.append(coords)

		#print self.final

		return self.final
		
	def get_adjacent_cells(self,cell):
		cells = []
    		if cell.x < self.grid_w-1:
    			cells.append(self.get_cell(cell.x+1, cell.y))
   		if cell.y > 0:
        		cells.append(self.get_cell(cell.x, cell.y-1))
    		if cell.x > 0:
        		cells.append(self.get_cell(cell.x-1, cell.y))
    		if cell.y < self.grid_h-1:
        		cells.append(self.get_cell(cell.x, cell.y+1))
#		if cell.x < self.grid_w-1 and cell.y<self.grid_h-1:
 #                       cells.append(self.get_cell(cell.x+1,cell.y+1))
  #              if cell.x < self.grid_w-1 and cell.y>0:
   #                     cells.append(self.get_cell(cell.x+1,cell.y-1))
    #            if cell.x >0 and cell.y<0:
     #                   cells.append(self.get_cell(cell.x-1,cell.y-1))
      #          if cell.x > 0 and cell.y<self.grid_h-1:
       #                 cells.append(self.get_cell(cell.x-1,cell.y+1))


    		return cells

	def update_cell(self,adj,cell):
		adj.g = cell.g + 1
    		adj.h = self.get_heuristic(adj)
    		adj.parent = cell
    		adj.f = adj.h + adj.g

	def get_heuristic(self, cell):
        	return 1 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y))	


#if __name__=="__main__":
	#a = Algo()
	#a.init_grid()
	#a.path_find()
	#a.display_path()
	#a.display_grid()
