import sys, os, time
import lcm
import math
 
from math import pi

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
 
import matplotlib.backends.backend_agg as agg

import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t
from lcmtypes import rplidar_laser_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

# Callling recently included files
from laser import *
from slam import *
from maps import *
from PIL import Image
from guidance import *

# SLAM preferences
# CHANGE TO OPTIMIZE
USE_ODOMETRY = True
MAP_QUALITY = 8

# Laser constants
# CHANGE TO OPTIMIZE IF NECSSARY
DIST_MIN = 100; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
# CHANGE TO OPTIMIZE
MAP_SIZE_M = 6.0 # size of region to be mapped [m]
INSET_SIZE_M = 2.5 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 5 # depth of data points on map (levels of certainty)

# CONSTANTS
DEG2RAD = math.pi / 180
RAD2DEG = 180 / math.pi

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules

g = Guidance()       
class MainClass:
    def __init__(self, width=640, height=480, FPS=10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
	
 
        # LCM Subscribe
        self.lc = lcm.LCM()
        # NEEDS TO SUBSCRIBE TO OTHER LCM CHANNELS LATER!!!
    	lcmLIDAR = self.lc.subscribe("RPLIDAR_LASER",self.LidarHandler)
    	lcmOdoVel = self.lc.subscribe("VEL",self.OdoVelocityHandler)
    	lcmOdoPos = self.lc.subscribe("ODO",self.OdoPositionHandler)
		
        # Prepare Figure for Lidar
        self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch, 
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.ax = self.fig.gca()

        # Prepare Figures for control
        path = os.path.realpath(__file__)
        path = path[:-17] + "maebotGUI/"

        self.arrowup = pygame.image.load(path + 'fwd_button.png')
        self.arrowdown = pygame.image.load(path + 'rev_button.png')
        self.arrowleft = pygame.image.load(path + 'left_button.png')
        self.arrowright = pygame.image.load(path + 'right_button.png')
        self.resetbut = pygame.image.load(path + 'reset_button.png')
        self.mapImg = pygame.Surface((250,250))
        
        
        self.arrows = [0,0,0,0]

    	self.thetas = []  
    	self.ranges = [] 
    	self.vel = (0.0,0.0,0.0)
    	self.pos = (0.0,0.0,0.0)
        self.init = False
	self.velCount = 0
        self.tempPos = (0.0,0.0,0.0)
        # PID Initialization - Change for your Gains!
        command = pid_init_t()
        command.kp = 0.0        
        command.ki = 0.0
        command.kd = 0.0
        command.iSat = 0.0 # Saturation of Integral Term. 
                           # If Zero shoudl reset the Integral Term
        self.lc.publish("GS_PID_INIT",command.encode())

        # Declare Laser, datamatrix and slam
        self.laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
        self.datamatrix = DataMatrix(**KWARGS) # handle map data
        self.slam = Slam(self.laser, **KWARGS) # do slam processing
 
    def OdoVelocityHandler(self,channel,data): 
 		msg = odo_dxdtheta_t.decode(data)
 		self.vel = (msg.dxy + self.vel[0],msg.dtheta + self.vel[1],msg.dt+self.vel[2])
		self.velCount = self.velCount + 1
		#print self.velCount,
 		#print "odo"

    def OdoPositionHandler(self,channel,data):
 		msg = odo_pose_xyt_t.decode(data)
 		self.pos = msg.xyt;
		
 		
 	
    def LidarHandler(self,channel,data): 
	# IMPLEMENT ME !!!
		msg = rplidar_laser_t.decode(data)
		self.nranges = msg.nranges
		self.ranges = msg.ranges
		self.thetas = msg.thetas #[x * RAD2DEG for x in msg.thetas]
		self.times = msg.times
		self.R = msg.intensities
                if self.velCount == 0:
                  self.velCount = 1
                #tempVel = [self.vel[0]/self.velCount, self.vel[1]*RAD2DEG/self.velCount, self.vel[2]/1000000.0/self.velCount]
	
                tempVel = [self.vel[0],self.vel[1]*RAD2DEG,self.vel[2]]
		#print self.velCount,
		#print "lidar"
	        points = zip([x*1000 for x in self.ranges],[x*RAD2DEG for x in self.thetas])
		#print self.nranges, len(points)
                self.tempPos = self.slam.updateSlam(points,tempVel)
		self.velCount = 0
		self.vel = (0,0,0)
               	#print "tempPos",
                #print self.tempPos
 

		if (self.init == False):
                  self.datamatrix.getRobotPos(self.tempPos,True)
		  g.start()
                  self.init = True
                else:
                  self.datamatrix.getRobotPos(self.tempPos)
		# Guidance
		g.guide_and_command((self.tempPos[0],self.tempPos[1],-((90+self.tempPos[2])*DEG2RAD)))
		self.datamatrix.drawBreezyMap(self.slam.getBreezyMap())
                self.datamatrix.drawInset()
		#self.datamatrix.saveImage()
                arr = np.rot90(np.fliplr(self.datamatrix.getInsetMatrix()),k=3)
                temp = np.asarray(arr,dtype=int)
                #pygame.surfarray.blit_array(self.mapImg,np.asarray(arr,dtype=int))
                #palette = [Color(i,i,i) for i in range(256)]
                #self.mapImg.set_palette(palette)
                #TODO
                #set_palette to fix grayscale

                

    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5
        #occupanyGrid = load('mapData/map1_3x3meters.npy')
        # Prepare Text to Be output on Screen
        font = pygame.font.SysFont("DejaVuSans Mono",14)

        while 1:
            leftVel = 0
            rightVel = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == KEYDOWN:
                    if ((event.key == K_ESCAPE)
                    or (event.key == K_q)):
                        sys.exit()
                    key = pygame.key.get_pressed()
                    if key[K_RIGHT]:
                        leftVel = leftVel + 0.40
                        rightVel = rightVel - 0.40
                    elif key[K_LEFT]:
                        leftVel = leftVel - 0.40
                        rightVel = rightVel + 0.40
                    elif key[K_UP]:
                        leftVel = leftVel + 0.45
                        rightVel = rightVel + 0.45
                    elif key[K_DOWN]:
                        leftVel = leftVel - 0.45
                        rightVel = rightVel - 0.45
                    else:
                        leftVel = 0.0
                        rightVel = 0.0                      
                    cmd = maebot_diff_drive_t()
                    cmd.motor_right_speed = vScale * rightVel
                    cmd.motor_left_speed = vScale * leftVel
                    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    command = velocity_cmd_t()
                    command.Distance = 987654321.0
                    if event.button == 1:
                        if ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            command.FwdSpeed = 150.0
                            command.Distance = 1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Forward One Meter!"
                        elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 150.0
                            command.Distance = -1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Backward One Meter!"
                        elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 35.0
                            command.Angle = 90.0
                            print "Commanded PID Left One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = -35.0
                            command.Angle = -90.0
                            print "Commanded PID Right One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            pid_cmd = pid_init_t()
                            pid_cmd.kp = 0.007        # CHANGE FOR YOUR GAINS!
                            pid_cmd.ki = 0.00001        # See initialization
                            pid_cmd.kd = 0.0008
                            pid_cmd.iSat = 0.0
                            self.lc.publish("GS_PID_INIT",pid_cmd.encode())
                            print "Commanded PID Reset!"
                        if (command.Distance != 987654321.0):
                            self.lc.publish("GS_VELOCITY_CMD",command.encode())

            self.screen.fill((255,255,255))

            # Plot Lidar Scans
            # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
            plt.cla()
            self.ax.plot(self.thetas,self.ranges,'or',markersize=2)
            self.ax.set_rmax(1.5)
            self.ax.set_theta_direction(-1)
            self.ax.set_theta_zero_location("N")
            self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                    labels=['','','','','','','',''], 
                                    frac=None,fmt=None)
            self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                    angle=None,fmt=None)

            canvas = agg.FigureCanvasAgg(self.fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            size = canvas.get_width_height()
            surf = pygame.image.fromstring(raw_data, size, "RGB")
            self.screen.blit(surf, (320,0))

            # Position and Velocity Feedback Text on Screen
            self.lc.handle()          
            pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
            text = font.render("    POSITION    ",True,(0,0,0))
            self.screen.blit(text,(10,360))
            text = font.render("x: " + str(round(self.tempPos[0],2)) +"[mm]",True,(0,0,0))
            self.screen.blit(text,(10,390))
            text = font.render("y: "+ str(round(self.tempPos[1],2)) +"[mm]",True,(0,0,0))
            self.screen.blit(text,(10,420))
            text = font.render("t: "+ str(round(self.tempPos[2],0))+"[deg]",True,(0,0,0))
            self.screen.blit(text,(10,450))
 
            #text = font.render("    VELOCITY    ",True,(0,0,0))
            #self.screen.blit(text,(150,360))
            #text = font.render("dxy/dt: " + str(round(self.vel[0],2)) + "[mm/s]",True,(0,0,0))
            #self.screen.blit(text,(150,390))
            #text = font.render("dth/dt: " + str(round(self.vel[1],2)) + " [deg/s]",True,(0,0,0))
            #self.screen.blit(text,(150,420))
            text = font.render("dt: " + str(round(self.pos[2]*RAD2DEG,2)) + " [s]",True,(0,0,0))
            self.screen.blit(text,(182,450))

            # Plot Buttons
            self.screen.blit(self.arrowup,(438,325))
            self.screen.blit(self.arrowdown,(438,400))
            self.screen.blit(self.arrowleft,(363,400))
            self.screen.blit(self.arrowright,(513,400))
            self.screen.blit(self.resetbut,(513,325))
            self.screen.blit(self.mapImg,(25,25))
            pygame.display.flip()


    
MainWindow = MainClass()
MainWindow.MainLoop()
