# odometry.py

# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# PLEASE CORRECT THE GYRODOMETRY FORMULAE TO CALCULATE THETA IN CALC VELOCITIES FUNCTION AND NOT IN SENSOR HANDLER

import sys, os, time
import lcm
from math import *

from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

class Maebot(WheeledRobot):
    
  def __init__(self):
    self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
    self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
    self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
    self.gearRatio = 30.0                    # 30:1 gear ratio
    self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]

    self.prevEncPos = (0,0,0)           # store previous readings for odometry
    self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
    self.currEncPos = (0,0,0)           # current reading for odometry
    self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
    self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
    self.gyro_offset = 0.0			# store values of gyro until offset is calculated
    self.gyro_int = 0	
    self.count = 0
    self.previousGyroTime = 0
    self.heading_theta = 0
    self.gyro = (0,0)
    WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

    # LCM Initialization and Subscription
    self.lc = lcm.LCM()
    lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
    lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA", self.sensorDataHandler)

    self.lcm_msg_counter = [0, 0]
  
  def calcVelocities(self):
    # IMPLEMENT ME
    # TASK: CALCULATE VELOCITIES FOR ODOMETRY
    # Update self.currOdoVel and self.prevOdoVel
    dxy = ((self.currEncPos[0] -self.prevEncPos[0]) + (self.currEncPos[1] - self.prevEncPos[1]))/2*self.enc2mm
    dtheta = ((self.currEncPos[1] -self.prevEncPos[1]) - (self.currEncPos[0] - self.prevEncPos[0]))/(self.axleLengthMillimeters)*self.enc2mm
    dt = (self.currEncPos[2] -self.prevEncPos[2])/1000000.0
    self.prevOdoVel = self.currOdoVel
    self.currOdoVel = (dxy, dtheta, dt)

  def getVelocities(self):
    # IMPLEMENT ME
    # TASK: RETURNS VELOCITY TUPLE
    # Return a tuple of (dxy [mm], dtheta [rad], dt [s])
    dxy = self.currOdoVel[0]
    dTGyro =  (self.gyro[1] - self.previousGyroTime)
    self.previousGyroTime = self.gyro[1]
        #self.gyrotheta = self.gyro_int # Store initial gyroint value 
    self.gyro = (self.gyro[0]/131,self.gyro[1] )
        #self.gyrotheta = self.gyro_int - self.gyrotheta # Compute dtheta using gyro integrated values

        #gyroDirection = list(msg.gyro)[2]

    if abs(self.gyro[0] - self.currOdoVel[1]/self.currOdoVel[2]) > 0.110:
     dtheta = self.gyro[0]*dTGyro*pi/180
     print "Went through gyro" + str(dtheta)
    else:
      dtheta = self.currOdoVel[1]

    
    #if theta > 360: 
      #theta = self.prevOdoPos[2] % 360
    #elif theta < -360:
      #theta = self.prevOdoPos[2] + 360

    dt = self.currOdoVel[2]
    return (dxy, dtheta, dt) # [mm], [rad], [s]

  def calcOdoPosition(self):
    # IMPLEMENT ME
    # TASK: CALCULATE POSITIONS
    # Update self.currOdoPos and self.prevOdoPos
    (dxy, dtheta, dt) = self.getVelocities()
    self.prevOdoPos = self.currOdoPos
    x = self.prevOdoPos[0] + dxy*cos((self.prevOdoPos[2]+dtheta))
    y =  self.prevOdoPos[1] + dxy*sin((self.prevOdoPos[2]+dtheta))


   # print("Gyro corrected is  : %f, offset calculated is %f"%(self.gyro,self.gyro_offset))

  
    theta = self.prevOdoPos[2] + dtheta
    print theta*180/pi
    self.currOdoPos = [x, y, theta]

  def getOdoPosition(self):
    # IMPLEMENT ME
    # TASK: RETURNS POSITION TUPLE
    # Return a tuple of (x [mm], y [mm], theta [rad])
    x = self.currOdoPos[0]
    y = self.currOdoPos[1]
    theta = self.currOdoPos[2]
    return (x, y, theta) # [mm], [rad], theta [rad]

  def publishOdometry(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_POSE MESSAGE
    msg = odo_pose_xyt_t()
    (msg.xyt[0],msg.xyt[1],msg.xyt[2])  =self. getOdoPosition()
    msg.utime = time.time()
    self.lc.publish("ODO", msg.encode())


  def publishVelocities(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
    msg = odo_dxdtheta_t()
    (msg.dxy,msg.dtheta,msg.dt)=self.getVelocities() 
    msg.utime = time.time()
    self.lc.publish("VEL", msg.encode())

  def motorFeedbackHandler(self,channel,data):
    msg = maebot_motor_feedback_t.decode(data)
    self.prevEncPos = self.currEncPos
    # IMPLEMENT ME
    # TASK: PROCESS ENCODER DATA
    # get encoder positions and store them in robot,
    # update robots position and velocity estimate
    self.currEncPos = [msg.encoder_left_ticks,msg.encoder_right_ticks,msg.utime_sama5]
    self.calcVelocities()
    self.calcOdoPosition()
    self.lcm_msg_counter[0] = self.lcm_msg_counter[0] - 1

  def sensorDataHandler(self,channel,data):
    msg = maebot_sensor_data_t.decode(data)
    countThreshold = 2000
    if self.count < countThreshold:
        self.gyro_offset = self.gyro_offset + list(msg.gyro)[2]
        self.count = self.count + 1
        self.previousGyroTime = msg.utime/1000000.0
	
    else:
        if self.count==countThreshold:
                self.gyro_offset = self.gyro_offset/countThreshold
                self.gyro = (list(msg.gyro)[2] - self.gyro_offset,msg.utime/1000000.0)
                self.count = self.count + 1
		self.previousGyroTime = msg.utime/1000000.0
		print "Done cal"
        else:
		#self.previousGyroTime = msg.utime/1000000.0
                self.gyro = (list(msg.gyro)[2] - self.gyro_offset,msg.utime/1000000.0)

    #self.lcm_msg_counter[1] = self.lcm_msg_counter[1] - 1      

   



    # IMPLEMENT ME
    # TASK: PROCESS GYRO DATA

  def MainLoop(self):
    oldTime = time.time()
    frequency = 20;
    while(1):
      self.lc.handle()
      if(time.time()-oldTime > 1.0/frequency):
      #if(self.lcm_msg_counter[0] * self.lcm_msg_counter[1] > 0):
        self.publishOdometry()
        self.publishVelocities()
        oldTime = time.time()
        self.lcm_msg_counter[0] = self.lcm_msg_counter[0] - 1
        self.lcm_msg_counter[1] = self.lcm_msg_counter[1] - 1


if __name__ == "__main__":
  
  robot = Maebot()
  robot.MainLoop()  
 
